<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 13.02.17
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="custom" uri="/WEB-INF/custom.tld" %>

<style>
    .pad{
        padding-top:25px;
    }
    .btn-margin{
        margin-left: 5px;
    }

    .filter .control-label{
        text-align: left;
    }
    .filter span{
        display: block;
    }
</style>
<script type="text/javascript">
    $(function() {
        $(".chosen-select").chosen();
    });
</script>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-3 col-xs-12">
            <form:form  action="/admin/book" method="GET" class="filter" modelAttribute="filter">
                <custom:hiddenInputs excludeParams="min,max,authorIds,categoryIds,bookLanguageIds,size,
                                                    _min,_max,_authorIds,_categoryIds,_bookLanguageIds,title,ISBN"/>


                <div class="form-group">
                    <div class="col-sm-6">
                    <form:input path="min" class="form-control" placeholder="Min"/>
                    </div>
                    <div class="col-sm-6">
                        <form:input path="max" class="form-control" placeholder="Max"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-12">Authors:</label>
                    <form:select path="authorIds" class="form-control chosen-select" items="${authors}" itemLabel="fullname" itemValue="id"/>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-12">Categories:</label>
                    <form:select path="categoryIds"
                                 items="${category}"
                                 class="form-control col-sm-8 chosen-select"
                                 itemLabel="name"
                                 itemValue="id"/>
                </div>
                <div class="form-group">
                    <div class="col-sm-12">
                        <label class="control-label col-sm-12">Book Languages:</label>
                        <form:checkboxes items="${book_language}" path="bookLanguageIds" itemLabel="name" itemValue="id"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-sm-12">Publishers:</label>
                    <div class="col-sm-12">
                        <form:checkboxes items="${publisher}" path="publisherIds" itemLabel="name" itemValue="id"/>
                    </div>
                </div>

                <div class="form-group">
                    <form:input type="text" class="form-control" path="title" placeholder="Title" id="bookTitle"/>
                </div>

                <div class="form-group">
                    <form:input type="text" class="form-control" path="ISBN" placeholder="ISBN" id="bookTitle"/>
                </div>

                <button type="submit" class="btn btn-primary">Ok</button>
            </form:form>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">Add Book:</span>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form:form action="/admin/book" modelAttribute="book" method="POST" enctype="multipart/form-data" class="form-horizontal">
                                <custom:hiddenInputs excludeParams="authors,book_language,title,ISBN, description,publisher,categories,price,publishYear"/>
                                <!--ADD AUTHOR!!!-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="authorIDS">Author(s): </label>
                                    <div class="col-sm-8">
                                            <form:select path="authors" itemValue="id"
                                                         itemLabel="fullname" items="${authors}" id="authorIDS" class="form-control col-sm-8 chosen-select"/>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="bookTitle" class="col-sm-4 control-label">Book title: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="title" id="bookTitle"/>
                                        <form:errors cssClass="req" path="title"/>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lang" class="col-sm-4 control-label">Language: </label>

                                    <div class="col-sm-8">
                                    <form:select path="book_language"
                                                 items="${book_language}"
                                                 id="lang"
                                                 class="form-control"
                                                 itemLabel="name"
                                                 itemValue="id"/>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="ISBN" class="col-sm-4 control-label">ISBN: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="ISBN" id="ISBN"/>
                                        <form:errors cssClass="req" path="ISBN"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="description" class="col-sm-4 control-label">Description:</label>
                                    <div class="col-sm-8">
                                        <form:textarea
                                                path="description"
                                                class="form-control"
                                                id="description" cols="50" rows="3"/>
                                        <form:errors cssClass="req" path="description"/>
                                    </div>
                                </div>


                                <div class="form-group">
                                    <label for="name" class="col-sm-4 control-label">Publisher: </label>

                                    <div class="col-sm-8">
                                        <form:select path="publisher"
                                                     items="${publisher}"
                                                     id="name"
                                                     class="form-control"
                                                     itemLabel="name"
                                                     itemValue="id"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Year" class="col-sm-4 control-label">Publish Year:</label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control"
                                                    path="publishYear" id="Year"/>
                                        <form:errors cssClass="req" path="publishYear"/>
                                    </div>
                                </div>

                                <!--Add Categories!-->
                                <div class="form-group">
                                    <label class="control-label col-sm-4" for="categoryIDS">Category(ies): </label>
                                    <div class="col-sm-8">

                                        <form:select path="categories"
                                                     items="${category}"
                                                     id="categoryIDS"
                                                     class="form-control col-sm-8 chosen-select"
                                        itemLabel="name"
                                        itemValue="id"/>
                                        <form:errors cssClass="req" path="categories"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Price" class="col-sm-4 control-label">Price: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="price" id="Price"/>
                                        <form:errors cssClass="req" path="price"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="file" class="col-sm-4 control-label">Image</label>
                                    <div class="col-sm-10">
                                        <input type="file" name="file" id="file">
                                    </div>
                                </div>


                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10 ">
                                        <button type="submit" class="btn btn-success form-control">Create</button>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pad">

                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">List of all books:</span>
                    <ul class="list-group">
                        <c:forEach items="${page.content}" var="books">
                            <li class="list-group-item clearfix ">
                                <img src="/images/book/${books.id}.jpg?version=
                                ${books.version}" width="10%">
                                <a href="/books/${books.id}">${books.title}</a>
                                <a class="btn btn-danger pull-right btn-margin" href="/admin/book/delete/${books.id}">Delete</a>
                                <a class="btn btn-warning pull-right"
                                   href="/admin/book/update/${books.id}">Update</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-xs-6 text-center">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button"
                                data-toggle="dropdown">
                            Sort <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <custom:sort innerHtml="Book asc" paramValue="Title" />
                            <custom:sort innerHtml="Book desc" paramValue="Title,desc" />

                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 text-center">
                    <custom:size posibleSizes="1,2,5,10" size="${page.size}" />
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 text-center">
        <custom:pageable page="${page}" cell="<li></li>"
                         container="<ul class='pagination'></ul>" />
    </div>
</div>