<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 13.02.17
  Time: 16:24
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/custom.tld" prefix="custom"%>


<style>
    .pad{
        padding-top:25px;
    }
    .btn-margin{
        margin-left: 5px;
    }
</style>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-3 col-xs-12">
                           <form:form class="form-inline" action="/admin/author" method="GET" modelAttribute="filter">
                    <custom:hiddenInputs excludeParams="search"/>
                    <div class="form-group">
                    <form:input path="search" class="form-control" placeholder="Search"/>
                </div>
                <button type="submit" class="btn btn-primary">Ok</button>
            </form:form>

        </div>

        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">Add authors:</span>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form:form action="/admin/author" method="POST" modelAttribute="author" class="form-horizontal">
                                <custom:hiddenInputs excludeParams="firstName,lastName"/>
                                <div class="form-group">
                                    <label class="col-sm-6 col-xs10 control-label" style="color: red">
                                    <form:errors cssClass="req" path="firstName"/>
                                        <br>
                                    <form:errors path="lastName"/>
                                </label>
                                </div>
                                <div class="form-group">
                                    <label for="firstName" class="col-sm-4 control-label">First Name: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="firstName" id="firstName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="lastName" class="col-sm-4 control-label">Last Name: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="lastName" id="lastName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10 ">
                                        <button type="submit" class="btn btn-success form-control">Create</button>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pad">

                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">List of all authors:</span>
                    <ul class="list-group">
                        <c:forEach items="${page.content}" var="authors">
                            <li class="list-group-item clearfix ">${authors.firstName} ${authors.lastName}
                                <a class="btn btn-danger pull-right btn-margin" href="/admin/author/delete/${authors.id}<custom:allParams/>">Delete</a>
                                <a class="btn btn-warning pull-right" href="/admin/author/update/${authors.id}<custom:allParams/>">Update</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">
            <div class="row">
                <div class="col-md-6 col-xs-6 text-center">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button"
                                data-toggle="dropdown">
                            Sort <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <custom:sort innerHtml="Author asc" paramValue="firstName" />
                            <custom:sort innerHtml="Author desc" paramValue="firstName,desc" />

                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 text-center">
                    <custom:size posibleSizes="1,2,5,10" size="${page.size}" />
                </div>
            </div>
        </div>
    </div>
</div>

</div>
<div class="row">
    <div class="col-md-12 col-xs-12 text-center">
        <custom:pageable page="${page}" cell="<li></li>"
                         container="<ul class='pagination'></ul>" />
    </div>
</div>
