<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 13.02.17
  Time: 16:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="/WEB-INF/custom.tld" prefix="custom"%>
<style>
    .pad{
        padding-top:25px;
    }
    .btn-margin{
        margin-left: 5px;
    }
</style>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-3 col-xs-12">
            <form:form class="form-inline" action="/admin/publisher" method="GET" modelAttribute="filter">
                <custom:hiddenInputs excludeParams="search"/>
                <div class="form-group">
                    <form:input path="search" class="form-control" placeholder="Search"/>
                </div>
                <button type="submit" class="btn btn-primary">Ok</button>
            </form:form>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">Add Publisher:</span>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form:form action="/admin/publisher" modelAttribute="publisher"
                                       method="POST" class="form-horizontal">
                                <custom:hiddenInputs excludeParams="name"/>
                                <div class="form-group">
                                    <label class="col-sm-4 col-xs10 control-label" style="color: red">
                                        <form:errors path="name"/>
                                    </label>
                                </div>
                                <div class="form-group">
                                    <label for="publisherName" class="col-sm-4 control-label">Publisher: </label>
                                    <div class="col-sm-8">
                                        <form:input type="text" class="form-control" path="name" id="publisherName"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10 ">
                                        <button type="submit" class="btn btn-success form-control">Create</button>
                                    </div>
                                </div>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pad">

                <div class="col-md-12 col-xs-12">
                    <span class="text-center h4">List of all name:</span>
                    <ul class="list-group">
                        <c:forEach items="${page.content}" var="publisher">
                            <li class="list-group-item clearfix ">${publisher.name}
                                <a class="btn btn-danger pull-right btn-margin" href="/admin/publisher/delete/${publisher.id}<custom:allParams/>">Delete</a>
                                <a class="btn btn-warning pull-right" href="/admin/puisherbl/update/${publisher.id}<custom:allParams/>">Update</a>
                            </li>
                        </c:forEach>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3 col-xs-12">

            <div class="row">
                <div class="col-md-6 col-xs-6 text-center">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button"
                                data-toggle="dropdown">
                            Sort <span class="caret"></span>
                        </button>
                        <ul class="dropdown-menu">
                            <custom:sort innerHtml="Publisher asc" paramValue="name" />
                            <custom:sort innerHtml="Publisher desc" paramValue="name,desc" />

                        </ul>
                    </div>
                </div>
                <div class="col-md-6 col-xs-6 text-center">
                    <custom:size posibleSizes="1,2,5,10" size="${page.size}" />
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-xs-12 text-center">
        <custom:pageable page="${page}" cell="<li></li>"
                         container="<ul class='pagination'></ul>" />
    </div>
</div>
