<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<div class="row" ng-controller="authorController">
    <div class="col-sm-3 col-xs-12">

        </div>

    <div class="col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                    <span class="text-center h4">Add authors:</span>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="firstName" class="col-sm-4 control-label">First Name: </label>
                                <div class="col-sm-8">
                                    <input type="text" required class="form-control" ng-minlength="3" ng-model="currentItem.firstName"  id="firstName"/>
                                    <span ng-messages="required">This field is required</span>
                                    <span ng-messages="minlength">Field must be at least 3 characters long</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="lastName" class="col-sm-4 control-label">Last Name: </label>
                                <div class="col-sm-8">
                                    <input type="text" required class="form-control" ng-model="currentItem.lastName" id="lastName"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10 ">
                                    <button type="button" ng-click="save()" class="btn btn-success form-control">Create</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <span class="h3">List of all authors:</span>
                <input type="text" class="form-control" ng-model="search" placeholder="Search">
                <p></p>
                <%--<ul class="list-group" >
                    <li class="list-group-item" ng-repeat="author in authors | filter : search | start: (currentPage - 1)
                                                                   * pageSize | limitTo: pageSize">
                        {{author.fullname}}
                        <button class="btn btn-danger pull-right btn-margin" ng-click="delete(author)">Delete</button>
                        <button class="btn btn-warning pull-right btn-margin" ng-click="update(author)">Update</button>
                    </li>
                </ul>--%>
                <table class="table">
                   <thread>
                       <tr>
                           <td>
                               <button class="btn btn-link" ng-click="sortType = 'firstName' sortReverse != sortReverse ">First name
                                   <span  ng-if="sortType == false"  ng-show="sortType == 'firstName'" class="glyphicon glyphicon-triangle-bottom"></span>
                                   <span ng-if="sortType == true" ng-show="sortType == 'firstName'" class="glyphicon glyphicon-triangle-top"></span>
                               </button>
                           </td>
                           <td>
                               <button class="btn btn-link" ng-click="sortType = 'lastName'">Last name
                                   <span ng-if="sortType == false" ng-show="sortType == 'lastName'" class="glyphicon glyphicon-triangle-bottom"></span>
                                   <span ng-if="sortType == true" ng-show="sortType == 'lastName'" class="glyphicon glyphicon-triangle-top"></span>
                               </button>
                           </td>
                           <td></td>
                       </tr>
                   </thread>

                    <tbody>
                    <tr ng-repeat="author in authors | orderBy:sortType:sortReverse">
                        <td>{{author.firstName}}</td>
                        <td>{{author.lastName}}</td>
                        <td>
                            <div class="btn-toolbar">
                                <button class="btn btn-danger pull-right btn-margin" ng-click="delete(author)">Delete</button>
                                <button class="btn btn-warning pull-right btn-margin" ng-click="update(author)">Update</button>
                            </div>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="row col-sm-12 col-xs-12">
            <ul uib-pagination
                total-items="authors.length" items-per-page="pageSize" ng-model="currentPage" max-size="5" class="pagination-sm">
            </ul>
        </div>
    </div>
    <div class="col-sm-3 col-xs-12">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><button class="btn btn-link" ng-model="fnasc">First name asc</button> </li>
                <li><button class="btn btn-link" ng-model="lnasc">Last name asc</button></li>
                <li><button class="btn btn-link" ng-model="fndesc">First name desc</button></li>
                <li><button class="btn btn-link" ng-model="lndesc">Last name desc</button></li>
            </ul>
        </div>
    </div>
</div>