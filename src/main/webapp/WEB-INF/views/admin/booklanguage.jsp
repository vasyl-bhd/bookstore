<%@ page pageEncoding="UTF-8" contentType="text/html;charset=UTF-8" language="java" %>
<div class="row" ng-controller="bookLanguageController">
    <div class="col-sm-3 col-xs-12">

    </div>

    <div class="col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <span class="text-center h4">Add book languages:</span>
                <div class="panel panel-default">
                    <div class="panel-body">
                        <form class="form-horizontal">
                            <div class="form-group">
                                <label for="name" class="col-sm-4 control-label">Language: </label>
                                <div class="col-sm-8">
                                    <input type="text" required class="form-control" ng-model="currentItem.name"  id="name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10 ">
                                    <button type="button" ng-click="save()" class="btn btn-success form-control">Create</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <span class="h3">List of all languages:</span>
                <input type="text" class="form-control" ng-model="search" placeholder="Search">
                <p></p>

                <table class="table">
                    <tr>
                        <th>Book Language:</th>
                    </tr>
                    <tr ng-repeat="lang in langs">
                        <td>
                            {{lang.name}}
                        </td>
                        <td>
                            <div class="btn-toolbar">
                                <button class="btn btn-danger pull-right btn-margin" ng-click="delete(lang)">Delete</button>
                                <button class="btn btn-warning pull-right btn-margin" ng-click="update(lang)">Update</button>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="row col-sm-12 col-xs-12">
            <pagination
                    total-items="langs.length" items-per-page="pageSize" ng-model="currentPage" max-size="5" class="pagination-sm">
            </pagination>
        </div>
    </div>
    <div class="col-sm-3 col-xs-12">
        <div class="dropdown">
            <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Sort By
                <span class="caret"></span></button>
            <ul class="dropdown-menu">
                <li><button class="btn btn-link" ng-model="fnasc">First name asc</button> </li>
                <li><button class="btn btn-link" ng-model="lnasc">Last name asc</button></li>
                <li><button class="btn btn-link" ng-model="fndesc">First name desc</button></li>
                <li><button class="btn btn-link" ng-model="lndesc">Last name desc</button></li>
            </ul>
        </div>
    </div>
</div>