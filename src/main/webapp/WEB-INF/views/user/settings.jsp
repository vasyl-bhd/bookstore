<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 28.03.17
  Time: 11:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-4 col-xs-12"></div>
    <div class="col-md-4 col-xs-12">
        <form:form action="/settings" method="post" modelAttribute="user"></form:form>
    </div>
    <div class="col-md-4 col-xs-12"></div>
</div>