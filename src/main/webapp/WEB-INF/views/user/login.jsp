<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 13.03.17
  Time: 16:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<c:if test="${param.fail}">
    	<div class="col-sm-12 col-xs-12 text-center">
            <span style="color: red;"> Email or password is incorrect.</span>
    	</div>
</c:if>
<div class="row">
    <div class="col-sm-3 col-xs-12"></div>
    <div class="col-sm-6 col-xs-12">
        <form:form class="form-horizontal" action="/login" method="POST" modelAttribute="user">
            <div class="form-group">
                <label for="login" class="col-sm-4 control-label" >Login: </label>
                <div class="col-sm-8 col-xs-12">
                    <input type="text" name="login" id="login" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <label for="login" class="col-sm-4 control-label" >Password: </label>
                <div class="col-sm-8 col-xs-12">
                    <input type="password" name="password" id="password" class="form-control">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-8 col-xs-12 col-sm-offset-2">
                    <label>
                        <input name="remember-me" type="checkbox"> Remember me
                    </label>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-success">Sign in</button>
                </div>
            </div>
        </form:form>
    </div>
    <div class="col-sm-3 col-xs-12"></div>
</div>