<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 13.03.17
  Time: 16:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row">

    <div class="col-sm-3 col-xs-12"></div>
    <div class="col-sm-6 col-xs-12">
        <div class="row">
            <div class="col-sm-12 col-xs-12">
                <span class="h2">Registration: </span>
            </div>
        </div>

        <form:form class="form-horizontal" action="/registration" method="POST" modelAttribute="user">
            <div class="form-group">
                <label for="firstName" class="col-sm-2 control-label" >
                    <span class="req">*</span>First Name:</label>
                <div class="col-sm-10">
                    <form:input path="firstName" cssClass="form-control" id="firstName"/>
                    <form:errors cssStyle="color: red;" path="firstName"/>
                </div>
            </div>
            <div class="form-group">
                <label for="lastName" class="col-sm-2 control-label" >Last Name:</label>
                <div class="col-sm-10">
                    <form:input path="lastName" cssClass="form-control" id="lastName"/>
                </div>
            </div>
            <div class="form-group">
                <label for="email" class="col-sm-2 control-label" >
                    <span class="req">*</span>E-mail:</label>
                <div class="col-sm-10">
                    <form:input path="email" cssClass="form-control" id="email"/>
                    <form:errors  cssStyle="color: red;" path="email"/>
                </div>
            </div>
            <div class="form-group">
                <label for="password" class="col-sm-2 control-label" >
                    <span class="req">*</span>Password:</label>
                <div class="col-sm-10">
                    <form:input type="password" path="password" cssClass="form-control" id="password"/>
                    <form:errors  cssStyle="color: red;" path="password"/>
                </div>
            </div>
            <div class="form-group">
                <label for="cPassword" class="col-sm-2 control-label" >
                    <span class="req">*</span>Password (confirm):</label>
                <div class="col-sm-10">
                    <form:input type="password" path="retypePassword" cssClass="form-control" id="cPassword"/>
                    <form:errors  cssStyle="color: red;" path="retypePassword"/>
                </div>
            </div>
            <div class="form-group">
                <span class="col-sm-offset-2 small"><span class="req">*</span> - This field is requied to fill!</span>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-12">
                    <button type="submit" class="btn btn-success">Register</button>
                </div>
            </div>
        </form:form>
    </div>
    <div class="col-sm-3 col-xs-12"></div>
</div>
