<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<div class="row">
    <div class="col-md-12 col-xs-12">
        <div class="col-md-3 col xs-12">

        </div>

        <div class="col-md-8 col xs-12">

            <c:forEach items="${book}" var="book">
            <div class="col-sm-4 col-lg-4 col-md-4">
            <div class="thumbnail">
                    <img src="/images/book/${book.id}.jpg?version=
                                ${book.version}" class="img-responsive" alt="">

                <div class="caption">

                    <h4 class="pull-right">$${book.price}</h4>
                    <h4><a href="/books/${book.id}">${book.title}</a>
                    </h4>
                    <button class="btn btn-success btn-xs">Add to cart</button>
                </div>

            </div>

        </div>
            </c:forEach>
    </div>
    </div>
</div>