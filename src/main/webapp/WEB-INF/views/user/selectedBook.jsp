<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 15.02.17
  Time: 12:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-1 col-xs-12"></div>
    <div class="col-md-9 col-xs-12">
        <h4>Item:</h4>
        <img src="/images/book/${book.id}.jpg?version=
                                ${book.version}" class="img-thumbnail img-responsive" width="150" alt="">

        <h2>${book.title}</h2>
        <br>
        Description:
        ${book.description}
        <br>
        Authors:
        <c:forEach items="${authors}" var="authors">
            <a href="selectedAuthors/${authors.id}">${authors.firstName} ${authors.lastName}</a>
        </c:forEach>
        <br>
        Categories:
        <c:forEach items="${categories}" var="categories">
            <a href="/categories/${categories.name.toLowerCase()}">
                    ${categories.name}</a>
        </c:forEach>
        <br>
        Language:
        ${book.getBook_language().name}
        <br>
        Publish Year:
        ${book.getPublishYear()}
    </div>
    <div class="col-md-1 col-xs-12"></div>
</div>