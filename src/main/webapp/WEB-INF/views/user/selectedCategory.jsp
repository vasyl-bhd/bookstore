<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: heniha3r
  Date: 15.02.17
  Time: 11:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="row">
    <div class="col-md-12 col-xs-12">

        <span class="h4">${bookCategory.name}</span>
        <ul>
            <c:forEach items="${book}" var="book">
                <li><a href="/books/${book.id}">${book.title}</a></li>
            </c:forEach>
            <c:if test="${empty book}">
                <h3>Category is empty</h3>
            </c:if>
        </ul>


    </div>
</div>