<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <script src="/resources/js/jquery-3.1.1.min.js"></script>
    <script src="/resources/js/chosen.jquery.min.js"></script>
    <script src="/resources/js/bootstrap.min.js"></script>
    <script src="/resources/js/angular.min.js"></script>
    <script src="/resources/js/angular-resource.js"></script>
    <script src="/resources/js/ngApp.js"></script>
    <script src="/resources/js/angularUI.js"></script>
    <script src="/resources/js/ngControllers/authorController.js"></script>
    <script src="/resources/js/ngControllers/bookLanguageController.js"></script>

    <link rel="stylesheet" type="text/css" href="/resources/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/resources/css/chosen.min.css">


    <style type="text/css">
        body {
            padding-bottom: 70px;
            padding-top: 70px;
        }
        @media (min-width: 1000px) {
            .navbar .navbar-nav {
                display: inline-block;
                float: none;
                vertical-align: top;
            }
            .navbar .navbar-collapse {
                text-align: center;
            }
        }
        @media(max-width:1000px)  {
            .nav > li{
                float: none;
                position: relative;
                display: block;
            }

        }
    </style>
    <link rel="stylesheet" href="/resources/css/style.css">
    <title><tiles:getAsString name="title"/></title>
</head>
<body  ng-app="app">
<tiles:insertAttribute name="header" />
<tiles:insertAttribute name="subHeader" />
<div class="container-fluid">
    <tiles:insertAttribute name="body" />
</div>
<tiles:insertAttribute name="footer" />

</body>
</html>