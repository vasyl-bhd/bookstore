<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<footer class="navbar navbar-default navbar-fixed-bottom">
    <div class="container-fluid">
        <div class="row text-center footer-padding">
            <span class="text-muted">©Bohdanets Vasyl in 2017</span>
        </div>
    </div>
</footer>