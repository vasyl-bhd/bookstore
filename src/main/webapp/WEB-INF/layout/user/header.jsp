<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>
<sec:csrfMetaTags/>
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <!--Navbar header-->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapsed" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Book Store</a>
        </div>
        <!--Navbar -->
        <div class="collapse navbar-collapse" id="navbar-collapsed">

            <sec:authorize access="isAuthenticated()">
                <sec:authentication var="principal" property="principal" />
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    ${principal.firstName}
                        <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <li><a href="/admin">Admin Page</a></li>
                        </sec:authorize>
                        <li><a href="#">Settings</a></li>
                    </ul>
                </li>

                <li>
                    <a href="/shoppingCart"><span class="glyphicon glyphicon-shopping-cart"></span>Shopping Cart</a>
                </li>
                <li>
                    <form:form action="/logout"  method="POST">
                        <button type="submit"  class="btn btn-danger logout"
                                style="margin: 9px 9px 0 0 ">Logout</button>
                    </form:form>
                </li>

            </ul>
            </sec:authorize>

            <sec:authorize access="!isAuthenticated()">
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="/login">Login</a></li>
                    <li><a href="/registration">Register</a></li>
                </ul>
            </sec:authorize>


            <form class="navbar-form navbar-search text-center" role="search">
                <div class="input-group">
                    <div class="input-group-btn">
                        <select name="search-type" class="form-control">
                            <option>All Categories</option>
                            <option>Author</option>
                            <option>Title</option>
                            <option>Category</option>
                        </select>

                    <input type="text" name="search" class="form-control">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-search btn-default">
                               <span class="glyphicon glyphicon-search"></span>
                         </button>

                         </div>
                    </div>
                </div>
            </form>

        </div>
    </div>
</nav>