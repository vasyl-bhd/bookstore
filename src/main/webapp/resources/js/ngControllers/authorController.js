

adminApp.controller('authorController',function ($scope, $http) {
    $scope.authors = [];
    $scope.currentItem = {};

    $scope.currentPage = 1;

    $scope.pageSize = 5;

    $scope.sortType = 'name';
    $scope.sortReverse = false;
    $scope.searchAuthor = '';
    
    $scope.cancel = function () {
        $scope.currentItem = {};
    };

    $scope.save = function () {

        $http({
            method: 'PUT',
            url: "/admin/restauthor",
            data: $scope.currentItem,
            headers:{'X-CSRF-TOKEN':$("meta[name='_csrf']").attr("content")}
        }).then(function (result) {
            for(var i = 0; i < $scope.authors.length; i++){
                if(result.data.id==$scope.authors[i].id){
                    $scope.authors.splice(i, 1);
                }
            }
            $scope.authors.push(result.data);
        });
        $scope.cancel();
    };

    $scope.update = function (item) {
        $scope.currentItem = item;

    };
    
    $scope.delete = function (item) {
        $http({
            method: "DELETE",
            url:"/admin/restauthor/" + item.id,
            headers:{'X-CSRF-TOKEN':$("meta[name='_csrf']").attr("content")}
        }).then(function () {
            for(var i = 0; i < $scope.authors.length; i++){
                if(item.id==$scope.authors[i].id){
                    $scope.authors.splice(i, 1);
                }
            }
        });
    };

    $scope.refresh = function () {
        $http({
            method: "GET",
            url:"/admin/restauthor"
        }).then(function (result) {
            $scope.authors = result.data;
        });
    };
    $scope.refresh();

}).filter('start', function () {
    return function (input, start) {
        if (!input || !input.length) { return; }

        start = +start;
        return input.slice(start);
    };
});



