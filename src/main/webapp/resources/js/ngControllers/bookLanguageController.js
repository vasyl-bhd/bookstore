adminApp.controller('bookLanguageController',function ($scope, $http) {
    $scope.langs = [];
    $scope.currentItem = {};

    $scope.currentPage = 1;

    $scope.pageSize = 5;

    $scope.cancel = function () {
        $scope.currentItem = {};
    };

    $scope.save = function () {
           var saveValid = true;
           for(var i = 0; i < $scope.langs.length; i++){
               if($scope.langs[i].name === currentItem.name){
                   saveValid = false;
                   alert("already exists");
               }
           }
           if(saveValid){
        $http({
            method: 'PUT',
            url: "/admin/restlang",
            data: $scope.currentItem,
            headers:{'X-CSRF-TOKEN':$("meta[name='_csrf']").attr("content")}
        }).then(function (result) {
            for(var i = 0; i < $scope.langs.length; i++){
                if(result.data.id==$scope.langs[i].id){
                    $scope.langs.splice(i, 1);
                }
            }
            $scope.langs.push(result.data);
        });
        $scope.cancel();
           }
    };

    $scope.update = function (item) {
        $scope.currentItem = item;

    };

    $scope.delete = function (item) {
        $http({
            method: "DELETE",
            url:"/admin/restlang/" + item.id,
            headers:{'X-CSRF-TOKEN':$("meta[name='_csrf']").attr("content")}
        }).then(function () {
            for(var i = 0; i < $scope.langs.length; i++){
                if(item.id==$scope.langs[i].id){
                    $scope.langs.splice(i, 1);
                }
            }
        });
    };

    $scope.refresh = function () {
        $http({
            method: "GET",
            url:"/admin/restlang"
        }).then(function (result) {
            $scope.langs = result.data;
        });
    };
    $scope.refresh();

}).filter('start', function () {
    return function (input, start) {
        if (!input || !input.length) { return; }

        start = +start;
        return input.slice(start);
    };
});



