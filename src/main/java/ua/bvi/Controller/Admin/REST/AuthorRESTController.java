package ua.bvi.Controller.Admin.REST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.bvi.Entity.Author;
import ua.bvi.Service.AuthorService;

import java.util.List;

/**
 * Created by Vasyl on 30.04.2017.
 */
@RestController
@RequestMapping(value = "/admin/restauthor")
public class AuthorRESTController {

    @Autowired
    private AuthorService authorService;

    @GetMapping
    public List<Author> findAll(){
        return authorService.findAll();
    }

    @PutMapping
    public Author save(@RequestBody Author author){
        return authorService.save(author);
    }

    @DeleteMapping("/{id}")
    public HttpStatus delete(@PathVariable int id){
        authorService.delete(id);
        return HttpStatus.OK;
    }

    @GetMapping("/id")
    public Author findOne(@PathVariable int id){
        return authorService.read(id);

    }
}
