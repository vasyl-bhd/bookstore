package ua.bvi.Controller.Admin.REST;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.BookLanguage;
import ua.bvi.Service.BookLanguageService;

import java.util.List;

/**
 * Created by Vasyl on 13.06.2017.
 */
@RestController
@RequestMapping("/admin/restlang")
public class BookLanguageRestController {

    @Autowired
    private BookLanguageService bls;

    @GetMapping
    public List<BookLanguage> findAll(){
        return bls.findAll();
    }

    @PutMapping
    public BookLanguage save(@RequestBody BookLanguage bookLanguage){
        return bls.save(bookLanguage);
    }

    @DeleteMapping("/{id}")
    public HttpStatus delete(@PathVariable int id){
        bls.delete(id);
        return HttpStatus.OK;
    }

    @GetMapping("/id")
    public BookLanguage findOne(@PathVariable int id){
        return bls.read(id);

    }


}
