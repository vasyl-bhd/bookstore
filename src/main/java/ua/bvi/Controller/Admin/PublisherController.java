package ua.bvi.Controller.Admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Publisher;
import ua.bvi.Service.PublisherService;
import ua.bvi.Validator.CategoryValidator;
import ua.bvi.Validator.PublisherValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/publisher")
@SessionAttributes("publisher")
public class PublisherController {

    @Autowired
    private
    PublisherService ps;

    @ModelAttribute("publisher")
    public Publisher getForm(){
        return new Publisher();
    }

    @ModelAttribute("filter")
    public SimpleFilter getFilter(){
        return new SimpleFilter();
    }

    @InitBinder("publisher")
    private void bind(WebDataBinder webDataBinder){
        webDataBinder.addValidators(new PublisherValidator(ps));
    }

    @GetMapping
    public String show(Model model,@PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("page",ps.findAll(pageable,filter));
        return "admin-publisher";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        ps.delete(id);
        return "redirect:/admin/publisher";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, Model model,
                         @PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("publisher",ps.read(id));
        show(model,pageable,filter);
        return "admin-publisher";
    }


    @PostMapping
    public String save(@ModelAttribute("publisher") @Valid Publisher publisher,
                       BindingResult br, Model model, SessionStatus status,
                       @PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        if(br.hasErrors()){
            return show(model,pageable,filter);
        }
        ps.create(publisher);
        status.setComplete();
        return "redirect:/admin/publisher";
    }
}
