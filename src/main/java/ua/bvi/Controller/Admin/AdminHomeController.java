package ua.bvi.Controller.Admin;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by heniha3r on 08.02.17 with project name bookstore
 */
@Controller
@RequestMapping("/admin")
public class AdminHomeController {

    @GetMapping
    public String index(){
        return "admin-index";
    }

    @GetMapping(value = "/rest")
    public String restAuthor(){
        return "admin-restauthor";
    }

    @GetMapping(value = "/booklanguage")
    public String bookLanguage(){
        return "admin-booklanguage";
    }


}
