package ua.bvi.Controller.Admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Category;
import ua.bvi.Service.CategoryService;
import ua.bvi.Validator.CategoryValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/category")
@SessionAttributes("category")
public class CategoryController {

    @Autowired
    private  CategoryService categoryService;

    @ModelAttribute("filter")
    public SimpleFilter getFilter(){
        return new SimpleFilter();
    }

    @ModelAttribute("category")
    public Category getForm(){
        return new Category();
    }

    @InitBinder("category")
    private void bind(WebDataBinder webDataBinder){
        webDataBinder.addValidators(new CategoryValidator(categoryService));
    }

    @GetMapping
    public String show(Model model,@PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("page",categoryService.findAll(pageable,filter));
        return "admin-category";
    }


    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        categoryService.delete(id);
        return "redirect:/admin/category";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, Model model,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("category",categoryService.read(id));
        show(model,pageable,filter);
        return "admin-category";
    }


    @PostMapping
    public String save(@ModelAttribute("category") @Valid Category cCategory,
                       BindingResult br, Model model, SessionStatus status,@PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        if(br.hasErrors()){
            return show(model,pageable,filter);
        }
            categoryService.create(cCategory);
            status.setComplete();
        return "redirect:/admin/category";
    }
}
