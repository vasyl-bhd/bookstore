package ua.bvi.Controller.Admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Author;
import ua.bvi.Service.AuthorService;
import ua.bvi.Validator.AuthorValidator;

import javax.validation.Valid;

import static ua.bvi.util.ParamBuilder.getParams;

@Controller
@RequestMapping("/admin/author")
@SessionAttributes("author")
public class AuthorController {

    @Autowired
    private
    AuthorService authorService;

    @InitBinder("author")
    protected void bind(WebDataBinder webDataBinder){
        webDataBinder.addValidators(new AuthorValidator(authorService));
    }

    @ModelAttribute("filter")
    public SimpleFilter getFilter(){
        return new SimpleFilter();
    }

    @ModelAttribute("author")
    public Author getForm(){
        return new Author();
    }
    
    

    @GetMapping
    public String show(Model model,@PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("page",authorService.findAll(pageable,filter));
        return"admin-author";
    }



    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, Model model,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("author",authorService.read(id));

        return show(model,pageable,filter);
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@PageableDefault Pageable pageable,@ModelAttribute("filter") SimpleFilter filter){
        authorService.delete(id);
        return "redirect:/admin/author" + getParams(pageable);
    }

    @PostMapping
    public String save(@ModelAttribute("author") @Valid Author author,
                       BindingResult br, Model model, SessionStatus status,
                       @PageableDefault Pageable pageable,@ModelAttribute("filter") SimpleFilter filter){
        if(br.hasErrors()) {
            return show(model,pageable,filter);
        }
        authorService.create(author);
        status.setComplete();
        return "redirect:/admin/author" + getParams(pageable);
    }



}
