package ua.bvi.Controller.Admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ua.bvi.DTO.Filter.BookFilter;
import ua.bvi.DTO.Form.BookForm;
import ua.bvi.Editor.*;
import ua.bvi.Entity.*;
import ua.bvi.Service.*;
import ua.bvi.Validator.BookValidator;
import ua.bvi.util.ParamBuilder;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/admin/book")
@SessionAttributes("book")
public class BookController {

    @Autowired
    private  BookService bookService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private BookLanguageService bookLanguageService;

    @Autowired
    private PublisherService publisherService;

    @InitBinder("book")
    protected void bind(WebDataBinder webDataBinder){
        webDataBinder.registerCustomEditor(Author.class,new AuthorEditor(authorService));
        webDataBinder.registerCustomEditor(Category.class,new CategoryEditor(categoryService));
        webDataBinder.registerCustomEditor(List.class,"categories",new SomeEditor(categoryService));
        webDataBinder.registerCustomEditor(BookLanguage.class, new BookLanguageEditor(bookLanguageService));
        webDataBinder.registerCustomEditor(Publisher.class,new PublisherEditor(publisherService));
        webDataBinder.setValidator(new BookValidator(bookService));
    }


    @ModelAttribute("book")
    public BookForm getForm(){
        return new BookForm();
    }

    @ModelAttribute("filter")
    public BookFilter getFilter(){
        return new BookFilter();
    }

    @GetMapping
    public String show(Model model, @PageableDefault Pageable pageable, @ModelAttribute("filter")
            BookFilter filter) {
        model.addAttribute("page", bookService.findAll(pageable,filter));
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("authors",authorService.findAll());
        model.addAttribute("book_language",bookLanguageService.findAll());
        model.addAttribute("publisher",publisherService.findAll());
        return "admin-book";
    }

    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, Model model,@PageableDefault Pageable pageable, @ModelAttribute("filter")
            BookFilter filter){
        model.addAttribute("book",bookService.findForm(id));
        return show(model,pageable,filter);
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@PageableDefault Pageable pageable, @ModelAttribute("filter")
            BookFilter filter){
        bookService.delete(id);
        return "redirect:/admin/book" + getParams(pageable,filter);
    }

    @GetMapping("/add/author/{id}")
    public String addAuthor(@PathVariable int id, Model model,@PageableDefault Pageable pageable, @ModelAttribute("filter")
            BookFilter filter){
        model.addAttribute("category",categoryService.findAll());
        model.addAttribute("author",authorService.findAll());
        return "admin-book";
    }

    @PostMapping
    public String save(@ModelAttribute("book") @Valid BookForm book,
                       BindingResult br, Model model, SessionStatus status,
                       @PageableDefault Pageable pageable, @ModelAttribute("filter")
                                   BookFilter filter){
        if(br.hasErrors()){
            return show(model,pageable,filter);
        }
        bookService.create(book);
        status.setComplete();
        return "redirect:/admin/book" + getParams(pageable,filter);
    }

    private String getParams(Pageable pageable, BookFilter filter){
        String page = ParamBuilder.getParams(pageable);
        StringBuilder builder = new StringBuilder(page);
        if(!filter.getMin().isEmpty()){
            builder.append("&min=");
            builder.append(filter.getMin());
        }
        if(!filter.getMax().isEmpty()){
            builder.append("&max=");
            builder.append(filter.getMax());
        }
        if(!filter.getAuthorIds().isEmpty()){
            for (Integer id :
                    filter.getAuthorIds()) {
                builder.append("&authorIds=");
                builder.append(id);
            }
        }
        if(!filter.getCategoryIds().isEmpty()){
            for (Integer id :
                    filter.getCategoryIds()) {
                builder.append("&authorIds=");
                builder.append(id);
            }
        }
        return builder.toString();
    }



}
