package ua.bvi.Controller.Admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.BookLanguage;
import ua.bvi.Service.BookLanguageService;
import ua.bvi.Validator.AuthorValidator;
import ua.bvi.Validator.BookLanguageValidator;

import javax.validation.Valid;

@Controller
@RequestMapping("/admin/booklanguages")
@SessionAttributes("booklanguage")
public class BookLanguageController {


    @Autowired
    private BookLanguageService bookLanguageService;

    @InitBinder("booklanguage")
    protected void bind(WebDataBinder webDataBinder){
        webDataBinder.addValidators(new BookLanguageValidator(bookLanguageService));
    }

    @ModelAttribute("filter")
    public SimpleFilter getFilter(){
        return new SimpleFilter();
    }

    @ModelAttribute("booklanguage")
    public BookLanguage getForm(){
        return new BookLanguage();
    }


    @GetMapping
    public String show(Model model,@PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("page",bookLanguageService.findAll(pageable,filter));
        return"admin-booklanguagse";
    }


    @GetMapping("/update/{id}")
    public String update(@PathVariable int id, Model model,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        model.addAttribute("booklanguage",bookLanguageService.read(id));
        show(model,pageable,filter);
        return "admin-booklanguage";
    }

    @GetMapping("/delete/{id}")
    public String delete(@PathVariable int id,@PageableDefault Pageable pageable,
                         @ModelAttribute("filter") SimpleFilter filter){
        bookLanguageService.delete(id);
        return "redirect:/admin/booklanguage";
    }


    @PostMapping
    public String save(@ModelAttribute("booklanguage") @Valid BookLanguage bookLanguage,
                       BindingResult br, Model model, SessionStatus status,
                       @PageableDefault Pageable pageable,
                       @ModelAttribute("filter") SimpleFilter filter){
        if(br.hasErrors()){
            return show(model,pageable,filter);
        }
        bookLanguageService.create(bookLanguage);
        status.setComplete();
        return "redirect:/admin/booklanguage";
    }

}
