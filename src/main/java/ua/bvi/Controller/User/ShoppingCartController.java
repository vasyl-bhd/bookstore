package ua.bvi.Controller.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ShoppingCartController {

    @GetMapping("/shoppingCart")
    public String cart(){
        return "user-shoppingCart";
    }
}
