package ua.bvi.Controller.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import ua.bvi.Entity.User;
import ua.bvi.Service.UserService;
import ua.bvi.Validator.RegistrationValidator;

import javax.validation.Valid;

@Controller
public class RegistrationController {


    @Autowired
    private UserService userService;

    @InitBinder("user")
    protected void bind(WebDataBinder webDataBinder){
        webDataBinder.addValidators(new RegistrationValidator(userService));
    }


    @GetMapping("/registration")
    public String show(Model model){
        model.addAttribute("user",new User());
        return "user-registration";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("user") @Valid User user,BindingResult bindingResult){
        if(bindingResult.hasErrors()){
            System.out.println("Error");
            return "user-registration";
        }
        userService.save(user);
        return "redirect:/login";
    }

}
