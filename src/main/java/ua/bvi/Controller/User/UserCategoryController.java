package ua.bvi.Controller.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ua.bvi.Service.BookService;
import ua.bvi.Service.CategoryService;

@Controller
public class UserCategoryController {

    @Autowired
    private
    CategoryService categoryService;

    @Autowired
    private BookService bookService;


    @GetMapping("/categories/{name}")
    public String categoriesSelector(@PathVariable String name, Model model){
        model.addAttribute("category",categoryService.findByName(name));
        model.addAttribute("book", bookService.findBookByCategory(name));
        return "user-selectedCategory";
    }
}
