package ua.bvi.Controller.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ua.bvi.Entity.User;
import ua.bvi.Service.AuthorService;
import ua.bvi.Service.BookService;
import ua.bvi.Service.CategoryService;
import ua.bvi.Service.UserService;

import javax.servlet.http.HttpServletResponse;
import java.security.Principal;


@Controller
public class UserHomeController {

    @Autowired
    private BookService bs;

    @Autowired
    private AuthorService authorService;


    @Autowired
    private CategoryService categoryService;

    @GetMapping("/")
    public String index (Model model){
        model.addAttribute("book",bs.findAll());
        model.addAttribute("categories",categoryService.findAll());
        return "user-index";
    }

    @GetMapping("/books/{id}")
    public String books(@PathVariable int id, Model model){
        model.addAttribute("book",bs.readOne(id));
        model.addAttribute("authors",authorService.findAuthorsById(id));
        model.addAttribute("categories",categoryService.findCategories(id));
        return "user-selectedBook";
    }


}
