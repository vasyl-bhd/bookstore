package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.DTO.Form.BookForm;
import ua.bvi.Service.BookService;

import java.util.regex.Pattern;

public class BookValidator implements Validator {
    private final static Pattern BIG_DECIMAL_REG = Pattern.compile
            ("^([0-9]{1,17}\\.[0-9]{1,2})|([0-9]{1,17}\\,[0-9]{1,2})|([0-9]{1,17})$");

    private final static Pattern YEAR_REG = Pattern.compile("[0-9]{3,4}");

    private final BookService bookService;

    public BookValidator(BookService bookService) {
        this.bookService = bookService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return BookForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BookForm bookForm = (BookForm) o;
        System.out.println("FILTER");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"title","",
                "Title must not be empty!");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"ISBN","",
                "ISBN must not be empty!");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"publishYear","",
                "Year must not be empty!");

        if(bookForm.getPrice().isEmpty()){
            System.out.println("Price Validation");
            errors.rejectValue("price","","Must not be empty!");

        }

        if(bookForm.getDescription().length() < 20){
            errors.rejectValue("description","","Description must be at least 20 characters");
        }


        if(bookForm.getDescription().length() >= 100000){
            errors.rejectValue("description","","Description must not be longer than 100000 characters");
        }


            if(bookService.findUnique(bookForm.getISBN()) != null){
                errors.rejectValue("ISBN","","Already Exists");
            }



        if(!YEAR_REG.matcher(bookForm.getPublishYear()).matches()){
            errors.rejectValue("publishYear","","Year must be only numbers(3 or 4 digits)!");
        }

        if(!BIG_DECIMAL_REG.matcher(bookForm.getPrice()).matches()) {
            System.out.println("price valid");
            errors.rejectValue("price", "", "Enter only numbers with . or ,");
        }

        if(bookForm.getCategories().isEmpty()){
            System.out.println("Cat valid");
            errors.rejectValue("categories","","Must be at least one category!");
        }
    }
}
