package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.Entity.Category;
import ua.bvi.Service.CategoryService;

public class CategoryValidator implements Validator {

    private final CategoryService categoryService;

    public CategoryValidator(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Category.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Category c = (Category) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name",""
                ,"Field Category should not be empty");

        if(categoryService.findByName(c.getName())!=null){
            errors.rejectValue("name","","AlreadyExists");
        }
    }
}
