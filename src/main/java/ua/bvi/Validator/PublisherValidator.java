package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.Entity.Publisher;
import ua.bvi.Service.PublisherService;

public class PublisherValidator implements Validator {

    private final PublisherService publisherService;

    public PublisherValidator(PublisherService publisherService) {
        this.publisherService = publisherService;
    }


    @Override
    public boolean supports(Class<?> aClass) {
        return Publisher.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Publisher p = (Publisher) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","",
                "Field Publisher should not be empty");

        if(publisherService.findByName(p.getName()) !=null){
            errors.rejectValue("name","","Already Exists");
        }
    }
}
