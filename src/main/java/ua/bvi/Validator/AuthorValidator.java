package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.Entity.Author;
import ua.bvi.Service.AuthorService;

public class AuthorValidator implements Validator {

    private final AuthorService authorService;

    public AuthorValidator(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return Author.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"firstName",
                "","Field First Name should not be empty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"lastName",
                "","Field Last Name should not be empty");


    }
}
