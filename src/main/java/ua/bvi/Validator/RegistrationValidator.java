package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.Entity.User;
import ua.bvi.Service.UserService;

public class RegistrationValidator implements Validator {

    private final String EMAIL_REGEXP = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";

    private final String PASSWORD_REGEXP = "([a-zA-Z0-9]{6,15})$";

    private final UserService userService;

    public RegistrationValidator(UserService userService) {
        this.userService = userService;
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"firstName",
                "","Field First Name should not be empty");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email",
                "","Please, enter email");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password",
                "","Please, enter your password");

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"retypePassword",
                "","Field First Name should not be empty");

        if(!user.getEmail().matches(EMAIL_REGEXP)){
            errors.rejectValue("email","","Please enter valid e-mail");

        }

        if(!user.getPassword().matches(PASSWORD_REGEXP)){
            errors.rejectValue("password","","Password must contain " +
                    "only english letters(Upper or Lower case, and numbers)!");
        }

        if(!user.getPassword().equals(user.getRetypePassword())){
            errors.rejectValue("retypePassword","","Passwords didn't match");
        }

        if(userService.findByEmail(user.getEmail())!= null){
            errors.rejectValue("email","","User with this email already exists");
        }


    }
}
