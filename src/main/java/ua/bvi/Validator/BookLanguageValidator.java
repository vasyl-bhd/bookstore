package ua.bvi.Validator;

import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import ua.bvi.Entity.BookLanguage;
import ua.bvi.Service.BookLanguageService;

public class BookLanguageValidator implements Validator {

    private final BookLanguageService bookLanguageService;

    public BookLanguageValidator(BookLanguageService bookLanguageService) {
        this.bookLanguageService = bookLanguageService;
    }



    @Override
    public boolean supports(Class<?> aClass) {
        return BookLanguage.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        BookLanguage a = (BookLanguage) o;
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name",
                "", "Field Language Name should not me empty");


        if(bookLanguageService.findByName(a.getName())!=null){
            errors.rejectValue("name","","Already Exists");
        }
    }
}
