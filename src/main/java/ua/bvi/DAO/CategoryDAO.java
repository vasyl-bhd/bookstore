package ua.bvi.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import ua.bvi.Entity.Book;
import ua.bvi.Entity.Category;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface CategoryDAO extends JpaRepository<Category,Integer>,JpaSpecificationExecutor<Category> {

    @Query("select c from Category c left join fetch c.books")
    List<Category> findAll();

    List<Book> findBookById(int id);

    Category findByName(String name);

    @Query("select distinct c from Category c left join fetch c.books b where b.id = ?1")
    List<Category> findCategoriesById(int id);
}
