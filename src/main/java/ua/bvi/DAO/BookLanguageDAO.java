package ua.bvi.DAO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ua.bvi.Entity.Book;
import ua.bvi.Entity.BookLanguage;
import ua.bvi.Specification.BookLanguageSpecification;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface BookLanguageDAO extends JpaRepository<BookLanguage,Integer>,JpaSpecificationExecutor<BookLanguage> {
    BookLanguage findByName(String name);

}
