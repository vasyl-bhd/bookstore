package ua.bvi.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import ua.bvi.Entity.Publisher;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface PublisherDAO extends JpaRepository<Publisher,Integer>,JpaSpecificationExecutor<Publisher> {
    Publisher findByName(String name);
}
