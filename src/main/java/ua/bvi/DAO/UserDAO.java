package ua.bvi.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import ua.bvi.Entity.User;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface UserDAO extends JpaRepository<User,Integer> {
    User findByEmail(String username);
}
