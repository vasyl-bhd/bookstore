package ua.bvi.DAO;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import ua.bvi.Entity.Author;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface AuthorDAO extends JpaRepository<Author,Integer>, JpaSpecificationExecutor<Author> {

    @Query("select distinct a from Author as a left join fetch  a.books b where b.id = ?1")
    List<Author> findAuthorsById(int id);



}
