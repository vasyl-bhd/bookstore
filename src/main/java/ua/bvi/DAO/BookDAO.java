package ua.bvi.DAO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.Book;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface BookDAO extends JpaRepository<Book,Integer>,JpaSpecificationExecutor<Book> {

    @Query("select DISTINCT b from Book b inner join  b.categories c where c.id = ?1")
    List<Book> findBookById(int id);

    @Query("select distinct b from Book b left join fetch b.categories c where LOWER(c.name)  = ?1")
    List<Book> findBookByCategory(String name);

    @Query("SELECT DISTINCT b FROM Book b left join fetch b.authors where b.id=:id")
    Book loadedAuthors(@Param("id") int id);

    @Query("SELECT DISTINCT b FROM Book b left join fetch b.categories where b.id=:id")
    Book loadedCategories(@Param("id") int id);



    @Query("SELECT b from Book b LEFT join fetch b.book_language left join fetch b.publisher " +
            "left join fetch b.categories where b.id = ?1")
    Book readOne(int id);

    @Query("SELECT b from Book b where b.ISBN = ?1")
    Book findUnique(String isbn);

}
