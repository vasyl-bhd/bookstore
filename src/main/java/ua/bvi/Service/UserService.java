package ua.bvi.Service;

import ua.bvi.Entity.User;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface UserService {
    void save(User user);
    User read(int id);
    void update(User user);
    void delete(int id);
    List<User> findAll();

    User findByEmail(String email);
}
