package ua.bvi.Service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Author;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface AuthorService {
    void create(Author author);
    Author read(int id);
    void update(Author author);
    void delete(int id);
    List<Author> findAll();
    Author save(Author author);

    List<Author> findAuthorsById(int id);



    Page<Author> findAll(Pageable pageable);
    Page<Author> findAll(Pageable pageable, SimpleFilter filter);

}
