package ua.bvi.Service;


import org.springframework.web.multipart.MultipartFile;

public interface FileWriter {
    enum Folder{
        BOOK
    }
    boolean write(Folder folder,MultipartFile file, Integer id);
}
