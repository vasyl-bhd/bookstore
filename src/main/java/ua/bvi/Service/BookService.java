package ua.bvi.Service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bvi.DTO.Filter.BookFilter;
import ua.bvi.DTO.Form.BookForm;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.Book;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface BookService {
    void create(BookForm book);
    Book read(int id);
    void update(Book book);
    void delete(int id);
    List<Book> findAll();
    BookForm findForm(int id);


    Book loadedAuthors(int id);

    List<Book> findBookByCategory(String name);

    Book readOne(int id);

    Page<Book> findAll(Pageable pageable, BookFilter filter);

    void save(Book entity);


    Book findUnique(String isbn);


}
