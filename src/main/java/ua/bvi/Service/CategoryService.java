package ua.bvi.Service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Book;
import ua.bvi.Entity.Category;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface CategoryService {
    void create(Category cat);
    Category read(int id);

    void update(Category cat);
    void delete(int id);
    List<Category> findAll();


    List<Category> findAll(List<Integer> collect);

    Category findByName(String name);

    Page<Category> findAll(Pageable pageable, SimpleFilter filter);

    List<Category> findCategories(int id);
}
