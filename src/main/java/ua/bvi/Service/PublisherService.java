package ua.bvi.Service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bvi.DAO.PublisherDAO;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Publisher;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface PublisherService {
    void create(Publisher publisher);
    Publisher read(int id);
    void update(Publisher publisher);
    void delete(int id);
    List<Publisher> findAll();

    Publisher findByName(String publisher);

    Page<Publisher> findAll(Pageable pageable, SimpleFilter filter);
}
