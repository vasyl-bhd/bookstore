package ua.bvi.Service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.Book;
import ua.bvi.Entity.BookLanguage;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
public interface BookLanguageService {
    void create(BookLanguage bl);
    BookLanguage read(int id);
    void update(BookLanguage bl);
    void delete(int id);
    List<BookLanguage> findAll();

    BookLanguage findByName(String name);

    Page<BookLanguage> findAll(Pageable pageable);
    Page<BookLanguage> findAll(Pageable pageable, SimpleFilter filter);

    BookLanguage save(BookLanguage bookLanguage);
}
