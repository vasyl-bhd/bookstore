package ua.bvi.DTO.Filter;

public class SimpleFilter {
    private String search = "";

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }
}
