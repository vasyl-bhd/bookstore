package ua.bvi.DTO.Form;

import org.springframework.web.multipart.MultipartFile;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.BookLanguage;
import ua.bvi.Entity.Category;
import ua.bvi.Entity.Publisher;

import java.util.List;

public class BookForm {
    private int id;

    private String title = "";

    private String description = "";

    private List<Category> categories;

    private List<Author> authors;

    private BookLanguage book_language;

    private Publisher publisher;

    private String ISBN = "";

    private String publishYear = "";

    private String price = "";

    private MultipartFile file;

    private int version;

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public BookLanguage getBook_language() {
        return book_language;
    }

    public void setBook_language(BookLanguage book_language) {
        this.book_language = book_language;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(String publishYear) {
        this.publishYear = publishYear;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }



}
