package ua.bvi.Specification;

import org.springframework.data.jpa.domain.Specification;
import ua.bvi.DTO.Filter.BookFilter;
import ua.bvi.Entity.*;

import javax.persistence.criteria.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

public class BookSpecification implements Specification<Book> {

    private final BookFilter bookFilter;

    private final List<Predicate> predicates = new ArrayList<>();

    private final static Pattern PATTERN = Pattern.compile
            ("^([0-9]{1,17}\\.[0-9]{1,2})|([0-9]{1,17}\\,[0-9]{1,2})|([0-9]{1,17})$");

    public BookSpecification(BookFilter bookFilter) {
        this.bookFilter = bookFilter;
        if(PATTERN.matcher(bookFilter.getMax()).matches()){
            bookFilter.setMaxPrice(new BigDecimal(bookFilter.getMax().replace(',','.')));
        }
        if(PATTERN.matcher(bookFilter.getMin()).matches()){
            bookFilter.setMinPrice(new BigDecimal(bookFilter.getMin().replace(',','.')));
        }
    }

    private void filterByTitle(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getTitle().isEmpty()){
            System.out.println(bookFilter.getTitle());
            predicates.add(cb.like(cb.lower(root.get(Book_.title)),
                    bookFilter.getTitle().toLowerCase()+"%"));
        }
    }

    private void filterByISBN(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getISBN().isEmpty()){
            predicates.add(cb.like(cb.lower(root.get(Book_.ISBN)),
                    bookFilter.getISBN().toLowerCase()+"%"));
        }
    }

    private void filterByLanguage(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getBookLanguageIds().isEmpty()){
            predicates.add(root.get("book_language").in(bookFilter.getBookLanguageIds()));
        }
    }

    private void filterByPublisher(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getPublisherIds().isEmpty()){
            predicates.add(root.get("publisher").in(bookFilter.getPublisherIds()));
        }
    }

    private void filterByAuthors(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getPublisherIds().isEmpty()){
            Join<Book,Author> catJoin = root.join("author");
            Predicate predicate = catJoin.get("id").in(bookFilter.getAuthorIds());
            predicates.add(predicate);
        }
    }

    private void filterByCategories(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(!bookFilter.getPublisherIds().isEmpty()){
            Join<Book,Category> catJoin = root.join("category");
            Predicate predicate = catJoin.get("id").in(bookFilter.getCategoryIds());
            predicates.add(predicate);
        }
    }

    private void filterByPrice(Root<Book> root, CriteriaQuery<?> query, CriteriaBuilder cb){
        if(bookFilter.getMaxPrice()!=null){
            predicates.add(cb.le(root.get("price"), bookFilter.getMaxPrice()));
        }
        if(bookFilter.getMinPrice()!=null){
            predicates.add(cb.ge(root.get("price"), bookFilter.getMinPrice()));
        }
    }



    private void fetch(Root<Book> root, CriteriaQuery<?> criteriaQuery){
        if(!criteriaQuery.getResultType().equals(Long.class)){
            criteriaQuery.distinct(true);
            root.fetch("book_language");
            root.fetch("publisher");
            root.fetch("categories");
        }
    }


    @Override
    public Predicate toPredicate(Root<Book> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        fetch(root,criteriaQuery);
        //filterByTitle(root,criteriaQuery,criteriaBuilder);
        filterByAuthors(root,criteriaQuery,criteriaBuilder);
        filterByCategories(root,criteriaQuery,criteriaBuilder);
        //filterByISBN(root,criteriaQuery,criteriaBuilder);
        filterByLanguage(root,criteriaQuery,criteriaBuilder);
        filterByPrice(root,criteriaQuery,criteriaBuilder);
        filterByPublisher(root,criteriaQuery,criteriaBuilder);
        if(predicates.isEmpty()) return null;
        Predicate[] array = new Predicate[predicates.size()];
        return criteriaBuilder.and(array);
    }
}
