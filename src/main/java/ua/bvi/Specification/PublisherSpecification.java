package ua.bvi.Specification;

import org.springframework.data.jpa.domain.Specification;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Publisher;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class PublisherSpecification implements Specification<Publisher> {

    private final SimpleFilter simpleFilter;

    public PublisherSpecification(SimpleFilter simpleFilter) {
        this.simpleFilter = simpleFilter;
    }

    @Override
    public Predicate toPredicate(Root<Publisher> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(simpleFilter.getSearch().isEmpty()) return null;
        return criteriaBuilder.like(criteriaBuilder.lower(root.get("name")),
                simpleFilter.getSearch().toLowerCase()+"%");
    }
}
