package ua.bvi.Specification;

import org.springframework.data.jpa.domain.Specification;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Author;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

public class AuthorSpecification implements Specification<Author> {

    private final SimpleFilter simpleFilter;

    public AuthorSpecification(SimpleFilter simpleFilter) {
        this.simpleFilter = simpleFilter;
    }

    @Override
    public Predicate toPredicate(Root<Author> root, CriteriaQuery<?> criteriaQuery, CriteriaBuilder criteriaBuilder) {
        if(simpleFilter.getSearch().isEmpty()) return null;
        return criteriaBuilder.like(criteriaBuilder.lower(root.get("firstName")),
        simpleFilter.getSearch().toLowerCase()+"%");
    }
}
