package ua.bvi.ServiceImplementation;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import ua.bvi.DAO.*;
import ua.bvi.DTO.Filter.BookFilter;
import ua.bvi.DTO.Form.BookForm;
import ua.bvi.Entity.*;
import ua.bvi.Service.BookService;
import ua.bvi.Service.FileWriter;
import ua.bvi.Specification.BookSpecification;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service
public class BookServiceImpl  implements BookService {

    @Autowired
    private BookDAO bookDAO;

    @Autowired
    private FileWriter fileWriter;




    @Override
    public void create(BookForm book) {
        Book entity = new Book();
        entity.setId(book.getId());
        entity.setTitle(book.getTitle());
        entity.setDescription(book.getDescription());
        entity.setCategories(book.getCategories());
        entity.setAuthors(book.getAuthors());
        entity.setBook_language(book.getBook_language());
        entity.setISBN(book.getISBN());
        entity.setPublishYear(book.getPublishYear());
        entity.setPublisher(book.getPublisher());
        entity.setPrice(new BigDecimal(book.getPrice().replace(',', '.')));
        MultipartFile file = book.getFile();
        entity = bookDAO.saveAndFlush(entity);
        System.out.println("LOG: Service Impl Create!");
        if(fileWriter.write(FileWriter.Folder.BOOK, file, entity.getId())){
            entity.setVersion(entity.getVersion()+1);
            bookDAO.save(entity);
        }
    }

    @Override
    public Book read(int id) {
        return bookDAO.findOne(id);
    }

    @Override
    public void update(Book book) {
        bookDAO.save(book);
    }

    @Override
    public void delete(int id) {
        bookDAO.delete(id);
    }

    @Override
    public List<Book> findAll() {
        return bookDAO.findAll();
    }




    @Override
    @Transactional
    public BookForm findForm(int id) {
        BookForm form = new BookForm();
        Book entity = readOne(id);
        form.setId(entity.getId());
        form.setTitle(entity.getTitle());
        form.setDescription(entity.getDescription());
        form.setCategories(entity.getCategories());
        form.setAuthors(entity.getAuthors());
        form.setBook_language(entity.getBook_language());
        form.setISBN(entity.getISBN());
        form.setPublishYear(entity.getPublishYear());
        form.setPublisher(entity.getPublisher());
        form.setPrice(String.valueOf(entity.getPrice()));
        form.setVersion(entity.getVersion());
        return form;

    }


    @Override
    public Book loadedAuthors(int id) {
        return loadedAuthors(id);
    }





    @Override
    public List<Book> findBookByCategory(String name) {
        return bookDAO.findBookByCategory(name);
    }

    @Override
    @Transactional
    public Book readOne(int id) {
        Book book = bookDAO.readOne(id);
        Hibernate.initialize(book.getAuthors());
        return book;
    }

    @Override
    public Page<Book> findAll(Pageable pageable, BookFilter filter) {
        return bookDAO.findAll(new BookSpecification(filter),pageable);
    }

    @Override
    public void save(Book entity) {
        bookDAO.save(entity);

    }



    @Override
    public Book findUnique(String isbn) {
        return bookDAO.findUnique(isbn);
    }



}
