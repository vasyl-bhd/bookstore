package ua.bvi.ServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.bvi.DAO.BookLanguageDAO;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.BookLanguage;

import ua.bvi.Service.BookLanguageService;
import ua.bvi.Specification.BookLanguageSpecification;


import java.math.BigDecimal;
import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service
public class BookLanguageServiceImpl implements BookLanguageService {

    @Autowired
    BookLanguageDAO bookLanguageDAO;

    @Override
    public void create(BookLanguage bl) {
        bookLanguageDAO.save(bl);
    }

    @Override
    public BookLanguage read(int id) {
        return bookLanguageDAO.findOne(id);
    }

    @Override
    public void update(BookLanguage bl) {
        bookLanguageDAO.save(bl);
    }

    @Override
    public void delete(int id) {
        bookLanguageDAO.delete(id);
    }

    @Override
    public List<BookLanguage> findAll() {
        return bookLanguageDAO.findAll();
    }

    @Override
    public BookLanguage findByName(String name) {
        return bookLanguageDAO.findByName(name);
    }

    @Override
    public Page<BookLanguage> findAll(Pageable pageable) {
        return bookLanguageDAO.findAll(pageable);
    }

    @Override
    public Page<BookLanguage> findAll(Pageable pageable, SimpleFilter filter) {
        return bookLanguageDAO.findAll(new BookLanguageSpecification(filter),pageable);
    }

    @Override
    public BookLanguage save(BookLanguage bookLanguage) {
        return bookLanguageDAO.saveAndFlush(bookLanguage);
    }
}
