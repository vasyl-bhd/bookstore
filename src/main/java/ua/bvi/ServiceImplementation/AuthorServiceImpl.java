package ua.bvi.ServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ua.bvi.DAO.AuthorDAO;
import ua.bvi.DAO.BookDAO;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Author;
import ua.bvi.Entity.Book;
import ua.bvi.Service.AuthorService;
import ua.bvi.Specification.AuthorSpecification;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service
public class AuthorServiceImpl implements AuthorService {

    @Autowired
    AuthorDAO authorDAO;

    @Autowired
    BookDAO bookDAO;

    @Override
    public void create(Author author) {
        authorDAO.save(author);
    }

    @Override
    public Author read(int id) {
        return authorDAO.findOne(id);
    }

    @Override
    public void update(Author author) {
        authorDAO.save(author);
    }

    @Override
    public void delete(int id) {
        authorDAO.delete(id);
    }

    @Override
    public List<Author> findAll() {
        return authorDAO.findAll();
    }

    @Override
    public Author save(Author author) {
        return authorDAO.saveAndFlush(author);
    }

    @Override
    public List<Author> findAuthorsById(int id) {
        return authorDAO.findAuthorsById(id);
    }


    @Override
    public Page<Author> findAll(Pageable pageable) {
        return authorDAO.findAll(pageable);
    }

    @Override
    public Page<Author> findAll(Pageable pageable, SimpleFilter filter) {
        return authorDAO.findAll(new AuthorSpecification(filter),pageable);
    }


}
