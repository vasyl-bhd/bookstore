package ua.bvi.ServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import ua.bvi.DAO.UserDAO;
import ua.bvi.Entity.Role;
import ua.bvi.Entity.User;
import ua.bvi.Service.UserService;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.Random;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service("userDetailsService")
public class UserServiceImpl implements UserService, UserDetailsService {

    @Autowired
    private UserDAO userDAO;

    @Autowired
 	private BCryptPasswordEncoder encoder;


    @Override
    public void save(User user) {
        user.setRole(Role.ROLE_USER);
        System.out.println("Role");
        user.setPassword(encoder.encode(user.getPassword()));
        System.out.println("encode");
        userDAO.save(user);
        System.out.println("saved");
    }

    @Override
    public User read(int id) {
        return userDAO.findOne(id);
    }

    @Override
    public void update(User user) {
        userDAO.save(user);
    }

    @Override
    public void delete(int id) {
        userDAO.delete(id);
    }

    @Override
    public List<User> findAll() {
        return userDAO.findAll();
    }

    @Override
    public User findByEmail(String email) {
        return userDAO.findByEmail(email);
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userDAO.findByEmail(s);
    }

    @PostConstruct
    public void addAdmin(){
        User user = userDAO.findByEmail("admin");
        if(user==null){
            user = new User();
            user.setEmail("admin");
            user.setPassword(encoder.encode("admin"));
            user.setFirstName("Admin#" + new Random().nextInt(1000));
            user.setRole(Role.ROLE_ADMIN);
            userDAO.save(user);
        }
    }
}
