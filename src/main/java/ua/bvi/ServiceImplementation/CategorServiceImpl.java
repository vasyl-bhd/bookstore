package ua.bvi.ServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.bvi.DAO.CategoryDAO;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Book;
import ua.bvi.Entity.Category;
import ua.bvi.Service.CategoryService;
import ua.bvi.Specification.CategorySpecification;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service
public class CategorServiceImpl implements CategoryService {
    @Autowired
    CategoryDAO categoryDAO;

    @Override
    public void create(Category cat) {
        categoryDAO.save(cat);
    }

    @Override
    public Category read(int id) {

        return categoryDAO.findOne(id);
    }

    @Override
    public void update(Category cat) {
        categoryDAO.save(cat);
    }

    @Override
    public void delete(int id) {
        categoryDAO.delete(id);
    }

    @Override
    public List<Category> findAll() {
        return categoryDAO.findAll();
    }


    @Override
    public List<Category> findAll(List<Integer> collect) {
        return categoryDAO.findAll(collect);
    }

    @Override
    public Category findByName(String name) {
        return categoryDAO.findByName(name);
    }

    @Override
    public Page<Category> findAll(Pageable pageable, SimpleFilter filter) {
        return categoryDAO.findAll(new CategorySpecification(filter),pageable);
    }

    @Override
    public List<Category> findCategories(int id) {
        return categoryDAO.findCategoriesById(id);
    }
}
