package ua.bvi.ServiceImplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import ua.bvi.DAO.PublisherDAO;
import ua.bvi.DTO.Filter.SimpleFilter;
import ua.bvi.Entity.Publisher;
import ua.bvi.Service.PublisherService;
import ua.bvi.Specification.PublisherSpecification;

import java.util.List;

/**
 * Created by heniha3r on 01.02.17 with project name JDBC
 */
@Service
public class PublisherServiceImpl implements PublisherService {

    @Autowired
    PublisherDAO publisherDAO;
    @Override
    public void create(Publisher publisher) {
        publisherDAO.save(publisher);
    }

    @Override
    public Publisher read(int id) {
       return publisherDAO.findOne(id);
    }

    @Override
    public void update(Publisher publisher) {
        publisherDAO.save(publisher);
    }

    @Override
    public void delete(int id) {
        publisherDAO.delete(id);
    }

    @Override
    public List<Publisher> findAll() {
        return publisherDAO.findAll();
    }

    @Override
    public Publisher findByName(String publisher) {
        return publisherDAO.findByName(publisher);
    }

    @Override
    public Page<Publisher> findAll(Pageable pageable, SimpleFilter filter) {
        return publisherDAO.findAll(new PublisherSpecification(filter),pageable);
    }
}
