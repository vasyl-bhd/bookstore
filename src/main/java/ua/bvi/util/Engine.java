package ua.bvi.util;

import org.imgscalr.Scalr;

import java.awt.*;
import java.awt.image.BufferedImage;

public class Engine {
    private final BufferedImage old;

    private final BufferedImage present;

    private final int oldX;

    private final int oldY;

    private final int expandSize;

    public Engine(BufferedImage old) {
        this.old = old;
        oldX = old.getWidth();
        oldY = old.getHeight();
        expandSize = Math.max(oldX,oldY);
        present = new BufferedImage(expandSize,expandSize,old.getType());
    }

    public BufferedImage expand(BufferedImage old){
        old = Scalr.resize(old,expandSize);
        return Scalr.pad(old,2, Color.WHITE);
    }




}
