package ua.bvi.util;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import ua.bvi.DTO.Filter.SimpleFilter;

public class ParamBuilder {
    public static String getParams(Pageable pageable) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("?page=");
        buffer.append(String.valueOf(pageable.getPageNumber() + 1));
        buffer.append("&size=");
        buffer.append(String.valueOf(pageable.getPageSize()));
        checkOnSort(pageable,buffer);
        return buffer.toString();
    }

    public static String getParams(Pageable pageable, SimpleFilter filter){
        StringBuilder buffer = new StringBuilder();
        buffer.append("?page=");
        buffer.append(String.valueOf(pageable.getPageNumber()+1));
        buffer.append("&size=");
        buffer.append(String.valueOf(pageable.getPageSize()));
        checkOnSort(pageable,buffer);
        if(!filter.getSearch().isEmpty()){
            buffer.append("&search=");
            buffer.append(filter.getSearch());
        }
        return buffer.toString();
    }

    private static void checkOnSort(Pageable pageable, StringBuilder buffer){
        if(pageable.getSort()!=null){
            buffer.append("&sort=");
            Sort sort = pageable.getSort();
            sort.forEach((order)->{
                buffer.append(order.getProperty());
                if(order.getDirection()!= Sort.Direction.ASC)
                    buffer.append(",desc");
            });
        }
    }
}
