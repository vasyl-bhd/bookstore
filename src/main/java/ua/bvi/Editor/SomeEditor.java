package ua.bvi.Editor;

import org.springframework.beans.factory.annotation.Autowired;
import ua.bvi.Entity.Category;
import ua.bvi.Service.CategoryService;

import java.beans.PropertyEditorSupport;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SomeEditor extends PropertyEditorSupport {

    private CategoryService categoryService;

    public SomeEditor(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {

        List<Category> categories = categoryService.findAll(Arrays.stream(text.split("\\,")).map(Integer::valueOf).collect(Collectors.toList()));
        setValue(categories);
    }
}
