package ua.bvi.Editor;

import ua.bvi.Entity.BookLanguage;
import ua.bvi.Service.BookLanguageService;

import java.beans.PropertyEditorSupport;

public class BookLanguageEditor extends PropertyEditorSupport {

    private final BookLanguageService bookLanguageService;


    public BookLanguageEditor(BookLanguageService bookLanguageService) {
        this.bookLanguageService = bookLanguageService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        BookLanguage bookLanguage = bookLanguageService.read(Integer.valueOf(text));
        setValue(bookLanguage);
    }
}
