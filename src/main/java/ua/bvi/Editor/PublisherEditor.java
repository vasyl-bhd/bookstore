package ua.bvi.Editor;

import ua.bvi.Entity.Publisher;
import ua.bvi.Service.PublisherService;

import java.beans.PropertyEditorSupport;

public class PublisherEditor extends PropertyEditorSupport {

    private final PublisherService publisherService;


    public PublisherEditor(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Publisher publisher = publisherService.read(Integer.valueOf(text));
        setValue(publisher);
    }
}
