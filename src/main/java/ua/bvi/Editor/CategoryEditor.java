package ua.bvi.Editor;


import ua.bvi.Entity.Category;
import ua.bvi.Service.CategoryService;

import java.beans.PropertyEditorSupport;

public class CategoryEditor extends PropertyEditorSupport {

    private final CategoryService categoryService;

    public CategoryEditor(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Category bookCategory = categoryService.read(Integer.valueOf(text));
        setValue(bookCategory);
    }
}
