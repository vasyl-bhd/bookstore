package ua.bvi.Editor;

import ua.bvi.Entity.Author;
import ua.bvi.Service.AuthorService;

import java.beans.PropertyEditorSupport;

public class AuthorEditor extends PropertyEditorSupport {


    private final AuthorService authorService;


    public AuthorEditor(AuthorService authorService) {
        this.authorService = authorService;
    }

    @Override
    public void setAsText(String text) throws IllegalArgumentException {
        Author author = authorService.read(Integer.valueOf(text));
        setValue(author);
    }
}
