package ua.bvi.Entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by Vasyl on 13.06.2017.
 */
@Entity
public class ShopCart {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToOne
    private User user;

    @ElementCollection
    @CollectionTable(name = "listOfUser")
    private List<Book> items;

    private BigDecimal totalPrice;

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<Book> getItems() {
        return items;
    }

    public void setItems(List<Book> items) {
        this.items = items;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShopCart shopCart = (ShopCart) o;

        if (id != shopCart.id) return false;
        if (user != null ? !user.equals(shopCart.user) : shopCart.user != null) return false;
        return items != null ? items.equals(shopCart.items) : shopCart.items == null;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (user != null ? user.hashCode() : 0);
        result = 31 * result + (items != null ? items.hashCode() : 0);
        return result;
    }
}
