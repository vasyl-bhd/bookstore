package ua.bvi.Entity;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heniha3r on 20.01.17 with project name JDBC
 */
@Entity
@Table(name = "Book",indexes = {@Index(columnList = "title"),@Index(columnList = "ISBN")})
public class Book {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToMany
    private List<Author> authors = new ArrayList<>();

    private  String title;

    @Column(length = 100000)
    private String description;

    @ManyToMany
    private List<Category> categories = new ArrayList<>();


    @ManyToOne(fetch=FetchType.LAZY)
    private BookLanguage book_language;

    @ManyToOne(fetch=FetchType.LAZY)
    private Publisher publisher;

    private String ISBN;

    private String publishYear;

    private BigDecimal price;

    private int version;

    public Book() {
    }

    public Book(String title) {
        this.title = title;
    }

    public Book(String title, String ISBN) {
        this.title = title;
        this.ISBN = ISBN;
    }

    public Book(String publishYear, String ISBN, String description, String title) {
        this.publishYear = publishYear;
        this.ISBN = ISBN;
        this.description = description;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Author> getAuthors() {
        return authors;
    }

    public void setAuthors(List<Author> authors) {
        this.authors = authors;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public BookLanguage getBook_language() {
        return book_language;
    }

    public void setBook_language(BookLanguage book_language) {
        this.book_language = book_language;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public String getPublishYear() {
        return publishYear;
    }

    public void setPublishYear(String publishYear) {
        this.publishYear = publishYear;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }


}
