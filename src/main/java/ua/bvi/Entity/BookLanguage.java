package ua.bvi.Entity;


import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by heniha3r on 20.01.17 with project name JDBC
 */
@Entity
@Table(name = "BookLanguage")
public class BookLanguage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;



    private String name;

    @OneToMany(mappedBy = "book_language")
    @JsonIgnore
    private List<Book> book = new ArrayList<>();

    public BookLanguage() {
    }

    public BookLanguage(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Book> getBook() {
        return book;
    }

    public void setBook(List<Book> book) {
        this.book = book;
    }

}
